from tkinter import Tk
import re
import pyperclip


# Get the clipboard content
clipboard_data = Tk().clipboard_get()
# Delete all doubled newlines
clipboard_data = clipboard_data.replace("\n\n", "\n")
# Delete all annotations
pattern = re.compile(r"\[\d*\]")
result = pattern.subn("", clipboard_data)
# Print the result
pyperclip.copy(result[0])
print(f"Removed {result[1]} annotations. The following text was copied to your clipboard")
print(result[0])
